apply:
	kubectl apply -f application.yaml

apply_test_app:
	kubectl config set-context --current --namespace=argocd
	argocd app create guestbook --repo https://github.com/argoproj/argocd-example-apps.git --path guestbook --dest-server https://kubernetes.default.svc --dest-namespace default

apply_wordpress_app:
	kubectl apply -f app-wordpress.yaml